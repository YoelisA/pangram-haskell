module Pangram (isPangram) where

import Data.Char
import Data.Function
import qualified Data.Set as Set

alphabet :: Set.Set Char
alphabet = ['a' .. 'z'] & Set.fromList

isPangram :: String -> Bool
isPangram text =
  let parsedText :: Set.Set Char
      parsedText = text & map toLower & filter (\x -> x `elem` ['a' .. 'z']) & Set.fromList
   in Set.difference alphabet parsedText & Set.null
